
public class AvionPasajeros extends Avion{
    //Atributo exclusivo de avion pasajeros
    private int pasajeros;

    public AvionPasajeros(String color, Double tamanio, int pasajeros){
        super(color, tamanio);
        this.pasajeros = pasajeros;
    }

    public void servir(){
        for(int i = 1; i <= pasajeros; i++){
            System.out.println("Atendiendo al pasajero numero " +i);
        }
    }
}
