public class App {
    public static void main(String[] args) throws Exception {

        AvionCarga objAvionCarga = new AvionCarga("Gris", 185.36);
        AvionPasajeros objAvionPasajeros = new AvionPasajeros("blue", 200.82, 120);
        AvionMilitar objAvionMilitar = new AvionMilitar("Azul con negro", 89.28, 20);
        objAvionPasajeros.servir();
    }
}
