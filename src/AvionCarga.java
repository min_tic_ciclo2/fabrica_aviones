public class AvionCarga extends Avion {
    
    /******
     * CONSTRUCTOR
     */
    public AvionCarga(String color, Double tamanio){
        super(color, tamanio);
    }

    /*****
     * METODOS
     */
    public void cargar(){
        System.out.println("cargando el avion... ");
    }

    public void descargar(){
        System.out.println("descargando... ");
    }
}
