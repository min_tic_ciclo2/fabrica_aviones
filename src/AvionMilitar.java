public class AvionMilitar extends Avion {
    
    private int misiles;

    public AvionMilitar(String color, Double tamanio){
        super(color, tamanio);
    }

    public AvionMilitar(String color, Double tamanio, int misiles){
        super(color, tamanio);
        this.misiles = misiles;
    }

    private void detectar_amenaza(boolean amezana){
        if(amenaza){
            this.disparar();
        }else{
            System.out.println("No es una amenaza");
        }
    }

    private void disparar(Integer misiles){
        if(this.misiles > 0){
            System.out.println("Disparando...");
            --this.misiles;
        }else{
            System.out.println("No hay munición");
        }
    }
}
