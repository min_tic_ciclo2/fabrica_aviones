public class Avion {
    /**
     * ATRIBUTOS CLASE PADRE
     */
    private String color;
    private double tamanio;

    /**
     * CONSTRUCTOR
     */
    public Avion (String color, Double tamanio){
    this.color = color;
    this.tamanio = tamanio;
    }

    /***
     * METODOS
     */
    public void aterrizar(){
        System.out.println("aterrizando... ");
    }

    public void despegar(){
        System.out.println("despegando... ");
    }

    public void frenar(){
        System.out.println("Frenando... ");
    }
}
